#!/bin/bash
##############################################################################
# USER INPUT VARIABLES
##############################################################################
GO_VERSION="${GO_VERSION:-""}"
GOROOT="${GOROOT:-"/usr/local/go"}"
GOBIN="${GOBIN:-""}"
UNINSTALL_CLEANLY="${UNINSTALL_CLEANLY:-""}"

##############################################################################
# APP VARIABLES
##############################################################################
VERSION="1.4.0"
action=""
current_path="$PWD"
config_path="/etc/profile.d/go.sh"
workspace="/tmp"
go_pack=""
go_url=""

##############################################################################
# FUNCTIONS LIBRARY
##############################################################################
_print_status() {
	_mode="$1"
	_message="${@:2}"

	case "$_mode" in
	error)
		1>&2 echo -e "[ ERROR   ] $_message"
		;;
	warning)
		1>&2 echo -e "[ WARNING ] $_message"
		;;
	*)
		1>&2 echo -e "[ INFO    ] $_message"
		;;
	esac

	unset _mode _message
}


##############################################################################
# PRIVATE FUNCTIONS
##############################################################################
_check_root_privilege() {
	if [ "$(id -u -n)" != "root" ]; then
		_print_status error "need root access for setup $GOROOT."
		_print_status error "need root access for setup $config_path."
		exit 1
	fi
}

_check_target_version() {
	if [ "$GO_VERSION" == "" ]; then
		_print_status error "target GO_VERSION not specified."
		exit 1
	fi
	_print_status info "Go $GO_VERSION requested. Installing..."
}

_configure_variables() {
	# detect CPU
	case "$(uname -m)" in
	x86_64)
		arch="amd64"
		;;
	i386|i686|x86|i686-AT386)
		arch="i386"
		;;
	aarch64)
		arch="arm64"
		;;
	armv5*)
		arch="armv5"
		;;
	armv6*)
		arch="armv6l"
		;;
	armv7*)
		arch="armv7"
		;;
	BePC)
		arch="bepc"
		;;
	ppc)
		arch="ppc"
		;;
	ppc64)
		arch="ppc64le"
		;;
	sparc64)
		arch="sparc64"
		;;
	*)
		_print_status error "unknown CPU architecture."
		exit 1
		;;
	esac
	_print_status info "detected architecture: $arch"

	# detect OS
	case "$(uname -s | tr '[:upper:]' '[:lower:]')" in
	darwin)
		machine="darwin"
		;;
	dragonfly)
		machine="dragonfly"
		;;
	freebsd)
		machine="freebsd"
		;;
	linux)
		machine="linux"
		;;
	android)
		machine="android"
		;;
	nacl)
		machine="nacl"
		;;
	netbsd)
		machine="netbsd"
		;;
	openbsd)
		machine="openbsd"
		;;
	plan9)
		machine="plan9"
		;;
	solaris)
		machine="solaris"
		;;
	windows)
		machine="windows"
		;;
	*)
		_print_status error "unknown operating system."
		exit 1
		;;
	esac
	_print_status info "detected operating system: $machine"

	# check supported packages
	case "$arch" in
	amd64)
		case "$machine" in
		darwin|linux|freebsd)
			;;
		windows)
			_print_status error "script not supported."
			exit 1
			;;
		*)
			_print_status error "unsupported operating system."
			exit 1
			;;
		esac
		;;
	386)
		case "$machine" in
		linux|freebsd)
			;;
		windows)
			_print_status error "script not supported."
			exit 1
			;;
		*)
			_print_status error "unsupported operating system."
			exit 1
			;;
		esac
		;;
	armv6l)
		case "$machine" in
		linux)
			;;
		*)
			_print_status error "unsupported operating system."
			exit 1
			;;
		esac
		;;
	arm64)
		case "$machine" in
		linux)
			;;
		*)
			_print_status error "unsupported operating system."
			exit 1
			;;
		esac
		;;
	ppc64le)
		case "$machine" in
		linux)
			;;
		*)
			_print_status error "unsupported operating system."
			exit 1
			;;
		esac
		;;
	s390x)
		case "$machine" in
		linux)
			;;
		*)
			_print_status error "unsupported operating system."
			exit 1
			;;
		esac
		;;
	*)
		_print_status error "unsupported cpu architecture."
		exit 1
		;;
	esac

	# formulate package and url variables
	go_pack="go${GO_VERSION}.${machine}-${arch}.tar.gz"
	go_url="https://dl.google.com/go/$go_pack"
	go_pack="${workspace}/${go_pack}"
	unset arch machine
}

_version() {
	echo "$@" | awk -F. '{ printf("%d%03d%03d%03d\n", $1,$2,$3,$4); }'
}

_get_latest_version_number() {
	if [ "$GO_VERSION" != "latest" ]; then
		return 0
	fi

	_print_status info "getting latest version number..."
	url="https://golang.org/dl/"
	GO_VERSION="0.0.0"

	ret=""
	if [ ! -z "$(type -p curl)" ]; then
		ret=$(curl --silent "$url")
	elif [ ! -z "$(type -p wget)" ]; then
		ret=$(wget --quiet -O - "$url")
	fi
	if [ $? -ne 0 ]; then
		_print_status error "error while reading https://golang.org/dl/"
		exit 1
	fi

	ret=$(echo "$ret" \
		| sed -n "/href/ s/.*href=['\"]\([^'\"]*\)['\"].*/\1/gp" \
		| grep "https://dl.google.com/go/" \
		| grep -v "beta" \
		| sed 's/https\:\/\/dl.google.com\/go\/go//g' \
		| sed 's/\.windows.*//' \
		| sed 's/\.darwin.*//' \
		| sed 's/\.linux.*//' \
		| sed 's/\.src.*//' \
		| sed 's/\.freebsd.*//' \
		| head -n 15 \
		| tail -n 11
	)

	newline="
"
	while [ ! -z "$ret" ]; do
		x="${ret%%$newline*}"
		if [ $(_version "$x") -ge $(_version "$GO_VERSION") ]; then
			GO_VERSION="$x"
		fi

		ret="${ret#*$x}"
		ret="${ret#*$newline}"
	done

	if [ "$GO_VERSION" = "0.0.0" ]; then
		_print_status error "unable to obtain latest number"
		exit 1
	fi

	_print_status info "got latest version: $GO_VERSION"
	unset x ret newline url crawler
}

_download_go_installer() {
	_print_status info "downloading package..."

	# download by choice
	if [ ! -z "$(type -p curl)" ]; then
		curl --silent \
			--fail \
			--location \
			--output "$go_pack" \
			--remote-name "$go_url"
	elif [ ! -z "$(type -p wget)" ]; then
		wget --https-only \
			--output-document="$go_pack" \
			"$go_url"
	fi
	unset go_url

	# check output
	if [ $? -ne 0 ] || [ ! -f "$go_pack" ]; then
		_print_status error "download installer package failed."
		exit 1
	fi

	_print_status info "success."
}

_setup_go() {
	_print_status info "unpacking package..."

	# unpack
	sudo tar -C "${GOROOT%/*}" -xzf "$go_pack"
	mkdir -p "${GOPATH}/src"
	mkdir -p "${GOPATH}/pkg"
	if [ ! -z "$GOBIN" ]; then
		mkdir -p "$GOBIN"
	fi

	# clean up
	rm "$go_pack"
	unset go_pack

	_print_status info "success."
}

_setup_config_path() {
	_print_status info "setting up $config_path"

	# write to path
	echo '#!/bin/sh
export GOROOT="${GOROOT:-"/usr/local/go"}"
export PATH="${PATH}:${GOROOT}/bin"
export GOPATH="${GOPATH:-"$(go env GOPATH)"}"
export PATH="${PATH}:${GOPATH}/bin"
if [ ! -z "$GOBIN" ]; then
	export PATH="${PATH}:${GOBIN}"
fi
' > "$config_path"

	# check output
	if [ ! -f "$config_path" ]; then
		_print_status warning "failed."
	fi

	# clean up
	unset config_path

	_print_status info "success."
}

_farewell_message() {
	_print_status info "Go $GO_VERSION installed successfully."
	_print_status info "
The environment variables (e.g. \$GOROOT and PATHing) will be available after
next login.

Otherwise, to avoid logout/login, you can:
1. Add 'source /etc/profile' in your ~/.bashrc
2. execute 'source /etc/profile' on this terminal

---
Enjoy programming in Go! See ya.
"
}

_remove_go() {
	_print_status info "removing $GOROOT..."

	sudo rm -rf "$GOROOT" &> /dev/null
	sudo rm -rf "$config_path" &> /dev/null

	_print_status info "success"
}

_remove_gopath_data() {
	_print_status info "removing GOBIN and GOPATH..."

	if [ ! -z "$GOBIN" ]; then
		sudo rm -rf "$GOBIN" &> /dev/null
	fi
	sudo rm -rf "$GOPATH" &> /dev/null

	_print_status info "success"
}


##############################################################################
# PUBLIC FUNCTIONS
##############################################################################
install() {
	_check_root_privilege
	_get_latest_version_number
	_check_target_version
	_configure_variables
	_remove_go
	_download_go_installer
	_setup_config_path
	_setup_go
	_farewell_message
}

uninstall() {
	_remove_go
	if [ "$UNINSTALL_CLEANLY" == "true" ]; then
		_remove_gopath_data
	fi
}

print_version() {
	echo $VERSION
}

bad_command() {
	_print_status error "invalid command."
	exit 1
}

print_help() {
	echo "\
GO INSTALLER AUTOMATOR
The semi-automatic script to install a usable Go Programming Language

This script needs root permission as it needs to install 2 things at system
locations:
1. go compiler --> /usr/local/go          = for compiling Go program
2. go.bash     --> /etc/profile.d/go.bash = for setting Go binaries into PATH

-------------------------------------------------------------------------------
To use: $0 [ACTION] [ARGUMENTS]

ACTIONS
1. -h, --help                   print help for this script app.

2. -i, --install [GO_VERSION]   install go into current operating system.
                                It takes 2 types of values:
                                  1. the version number - specific version
                                  2. latest             - scan for latest
                                                          version from the web.
                                EXAMPLES:
                                  1. $ ./gosetup.bash -i 1.12.7
                                  2. $ ./gosetup.bash --install 1.12.7
                                  3. $ export GO_VERISON=1.12.7 && \\
                                       ./gosetup.bash -i
                                  4. $ ./gosetup.bash -i latest
                                  5. $ ./gosetup.bash --install latest

3. -u, --uninstall [CLEANLY]    uninstall go from the system. If cleanly is
                                set, this app will remove the GOPATH and GOBIN,
                                erasing zero known traces of Go.
                                EXAMPLES:
                                  1. $ ./gosetup.bash -u
                                  2. $ ./gosetup.bash --uninstall
                                  3. $ ./gosetup.bash -u cleanly
                                  4. $ ./gosetup.bash --uninstall cleanly
                                  3. $ export UNINSTALL_CLEANLY=true && \\
                                       ./gosetup.bash -u

4. -v, --version                print this app version.
"
}

##############################################################################
# MAIN CLI
##############################################################################
run_action() {
case "$action" in
"h")
	print_help
	;;
"i")
	install
	;;
"u")
	uninstall
	;;
"v")
	print_version
	;;
*)
	bad_command
	;;
esac
}

process_parameters() {
while [[ $# != 0 ]]; do
case "$1" in
-h|--help)
	action="h"
	;;
-i|--install)
	action="i"
	if [[ "$2" != "" && "${2:1}" != "-" ]]; then
		GO_VERSION="$2"
		shift 1
	fi
	;;
-u|--uninstall)
	action="u"
	if [[ "$2" != "" && "$2" == "cleanly" ]]; then
		UNINSTALL_CLEANLY="true"
		shift 1
	fi
	;;
-v|--version)
	action="v"
	;;
*)
	;;
esac
shift 1
done
}

main() {
	process_parameters $@
	run_action
}

if [[ $BASHELL_TEST_ENVIRONMENT != true ]]; then
	main $@
fi
