#!/bin/bash
##############################################################################
# USER INPUT VARIABLES
##############################################################################
TEST_DIRECTORY="${TEST_DIRECTORY:-"/tmp"}"
GO_VERSION="${GO_VERSION:-""}"
GOLANGCI_LINT_VERSION="${GOLANGCI_LINT_VERSION:-"v1.21.0"}"
script_directory=".scripts"
profile_path="/etc/profile.d/go.sh"
config_filename="goConfig.bash"
setup_filename="goSetup.bash"
go_coverage_mode="count"

##############################################################################
# APP VARIABLES
##############################################################################
VERSION="1.5.0"
action=""
repo_name=""
langauge=""
module_directory=""
script_path=""
bin_directory="${TEST_DIRECTORY}/gobin"
work_directory="${TEST_DIRECTORY}/work"
results_directory="${TEST_DIRECTORY}/results"
coverage_profile="${results_directory}/profiles"

##############################################################################
# FUNCTIONS LIBRARY
##############################################################################
_print_status() {
	mode="$1"
	message="${@:2}"

	case "$mode" in
	error)
		1>&2 echo -e "[ ERROR   ] $message"
		;;
	warning)
		1>&2 echo -e "[ WARNING ] $message"
		;;
	*)
		1>&2 echo -e "[ INFO    ] $message"
		;;
	esac

	unset mode message
}

##############################################################################
# PRIVATE FUNCTIONS
##############################################################################
_download_gitlabci-lint() {
	url="https://raw.githubusercontent.com/golangci/golangci-lint"
	url="${url}/master/install.sh"

	if [ ! -z "$(type -p curl)" ]; then
		curl -sfL "$url" \
			| sh -s -- -b "$bin_directory" "$GOLANGCI_LINT_VERSION"
	elif [ ! -z "$(type -p curl)" ]; then
		wget -O - -q "$url" \
			| sh -s -- -b "$bin_directory" "$GOLANGCI_LINT_VERSION"
	fi

	unset url
}

_install_go() {
	sudo "$1" -i "$GO_VERSION"
	if [ $? != 0 ]; then
		_print_status error "failed to install go compiler."
		exit 1
	fi

	if [ ! -f "$profile_path" ]; then
		_print_status error "missing profile path: $profile_path."
		exit 1
	fi
	source "$profile_path"
}

_prepare_workspace() {
	rm -rf "$bin_directory" &> /dev/null
	rm -rf "$work_directory" &> /dev/null
	rm -rf "$results_directory" &> /dev/null
	mkdir -p "$bin_directory"
	mkdir -p "$work_directory"
	mkdir -p "$results_directory"

	# set pathing for go binary downloads
	PATH="${PATH}:${bin_directory}"

	# scan for module root path
	while true; do
		if [ -z "$s" ]; then
			if [ -z "$module_directory" ]; then
				module_directory="$PWD"
			fi
			break
		fi

		if [ -f "${p}/go.mod" ]; then
			module_directory="$p"
			break
		fi

		p="${s%/*}"
	done

	# setup go
	script_path="${module_directory}/${script_directory}/${setup_filename}"
	if [ ! -f "$script_path" ]; then
		_print_status error "missing setup script: $script_path."
		exit 1
	fi
	_install_go "$script_path"


	# download dependencies
	_download_gitlabci-lint

	# source go configuration run
	script_path="${module_directory}/${script_directory}/${config_filename}"
	if [ ! -f "$script_path" ]; then
		_print_status error "missing config script: $script_path."
		exit 1
	fi
	source "$script_path"
}

_get_coverage_data() {
	test_verdict=0
	packages=($(go list ./...))

	# run test
	echo -e "\n\n"
	echo "BEGIN CERIGO TEST COVERAGE"
	printf '=%.0s' {1..79} && echo ""

	for package in "${packages[@]}"; do
		subject="${work_directory}/${package//\//-}.cover"

		go test -timeout 20m \
			-covermode="$go_coverage_mode" \
			-coverprofile="$subject" \
			"$package"

		if [ $? -ne 0 ]; then
			test_verdict=1
		fi
	done
	echo "mode: $go_coverage_mode" > "$coverage_profile"
	grep -h -v "^mode:" "$work_directory"/*.cover >> "$coverage_profile"
	unset packages package subject
	go tool cover -func="$coverage_profile"

	printf '=%.0s' {1..79} && echo ""
	echo -e "END CERIGO TEST COVERAGE"

	# return positive case
	if [ $test_verdict -eq 0 ]; then
		unset test_verdict
		return 0
	fi

	# fail by default
	unset test_verdict
	return 1
}

_static_analysis() {
	if [ "$(type goCheck 2> /dev/null)" == "" ]; then
		_print_status error "goCheck is not configured properly."
		exit 1
	fi
	goCheck

	if [ -f "$GO_CHECK_LOG" ]; then
		cat "$GO_CHECK_LOG"
	fi
}

##############################################################################
# PUBLIC FUNCTIONS
##############################################################################
run() {
	_prepare_workspace
	_static_analysis
	_get_coverage_data
	if [ $? -ne 0 ]; then
		exit 1
	fi
}

call_report_card() {
	if [ -z "$(type -p curl)" ]; then
		_print_status error "curl program is missing."
		exit 1
	fi

	if [ -z "$repo_name" ]; then
		_print_status error "empty repo_name."
		exit 1
	fi

	url="https://goreportcard.com/checks"
	_print_status info "calling goreportcard.com for ${repo_name}..."
	repo_name="${repo_name////%2F}"

	curl --fail --location --silent \
		--header "Content-Type: application/x-www-form-urlencoded" \
		--data "repo=$repo_name" \
		--request POST "$url" > /dev/null
	ret=$?
	unset url repo_name

	if [ $ret -eq 0 ]; then
		unset ret
		_print_status info "[ SUCCESS ]"
		exit 0
	fi

	unset ret
	_print_status info "[ FAILED  ]"
	exit 1
}

print_regex() {
	case "$language" in
	ruby)
		echo -e "total:\s{1,}\(statements\)\s{1,}(\d+.\d+%)"
		;;
	*)
		_print_status error "missing langauge argument."
		exit 1
		;;
	esac
}

print_version() {
	echo "$VERSION"
}

print_help() {
	echo "\
GO CI AUTOMATOR
The semi-automatic script for automating Go testing and test coverage for
single or multiple packages.
-------------------------------------------------------------------------------
To use: $0 [ACTION] [ARGUMENTS]

ACTIONS
1. -h, --help                   print help.

2. -r, --run                    run the test.

3. -rc, --report-card           call the goreportcard.com to run a test
                                for the given repository.
                                COMPULSORY VALUE:
                                1. -rc <repo import path>
                                        the repository import path.
                                EXAMPLE:
                                1. $ ./program -rc gitlab.com/zoralab/cerigo

4. -re, --ruby-regex            return the regex for parsing coverage
                                statistic.
                                COMPULSORY VALUE:
                                1. -re <langauge>
                                        the language
                                EXAMPLE:
                                1. $ ./program -re ruby

5. -v, --version                print this app version.
"
}

##############################################################################
# MAIN CLI
##############################################################################
run_action() {
case "$action" in
"h")
	print_help
	;;
"r")
	run
	;;
"rc")
	call_report_card
	;;
"re")
	print_regex
	;;
"v")
	print_version
	;;
*)
	_print_status error "invalid command."
	exit 1
	;;
esac
}

process_parameters() {
while [ $# != 0 ]; do
case "$1" in
-h|--help)
	action="h"
	;;
-r|--run)
	action="r"
	;;
-rc|--report-card)
	if [[ "$2" != "" && "${2:0:1}" != "-" ]]; then
		repo_name="$2"
		shift 1
	fi
	action="rc"
	;;
-re|--regex)
	action="re"
	case "$2" in
	ruby|Ruby|RUBY)
		language="ruby"
		;;
	*)
		language=""
		;;
	esac
	;;
-v|--version)
	action="v"
	;;
esac
shift 1
done
}

main() {
	process_parameters $@
	run_action
}

if [[ "$BASHELL_TEST_ENVIRONMENT" != true ]]; then
	main $@
fi
