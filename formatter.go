package godocgen

import (
	"bufio"
	"os"
	"path/filepath"

	"gitlab.com/zoralab/godocgen/internal/renderer"
)

type formatter struct {
	pkg          *Data
	outputPath   string
	templatePath string
	filename     string
	width        uint

	canva  *renderer.Canva
	writer *bufio.Writer
	file   *os.File

	testConfig map[int]bool
}

func (f *formatter) openWriter() (err error) {
	var file *os.File
	var docType string

	// adjust terminal output docType
	if f.outputPath == Terminal {
		f.filename = renderer.DocTypeTerminal
	}

	// check supported format
	docType = renderer.IsSupportedFormat(f.filename)
	if docType == "" {
		return createError(ErrorUnknownOutputFormat, f.pkg.ImportPath)
	}

	// create appropriate writer (default to terminal)
	file = os.Stdout
	if f.pkg.Filepath != "" {
		file, err = f.createFile(f.pkg.Filepath)
		if err != nil {
			return err
		}

		f.file = file
	}

	f.writer = bufio.NewWriter(file)

	// create canva
	f.canva, err = renderer.New(f.writer, f.templatePath, docType, f.width)
	if err != nil || f.testConfig[testSimulateFailedCanvaCreation] {
		f.closeWriter()
		_ = os.RemoveAll(f.pkg.Filepath)

		return createError(ErrorInTemplate, f.templatePath)
	}

	return nil
}

func (f *formatter) closeWriter() {
	f.writer.Flush()

	if f.file != nil {
		f.file.Close()
	}
}

func (f *formatter) renderPackage() {
	f.canva.ParseHead(f.pkg.Header, f.pkg.Synopsis, f.pkg.RelativePath)
	f.canva.ParseTitle(headerTitle, f.pkg.Header)
	f.canva.ParseImportPath(f.pkg.ImportPath)
	f.canva.ParseSynopsis(f.pkg.Synopsis)
	f.renderExample(f.pkg.Examples.Examples, headerExample, true)
	f.renderDecl(f.pkg.Constants.List, headerConstant)
	f.renderDecl(f.pkg.Variables.List, headerVariable)
	f.renderFunction(f.pkg.Functions.List, headerFunction, false)
	f.renderType(f.pkg.Types)
}

// |---------------------------------------------------------|
// |-- elemental private functions                         --|
// |---------------------------------------------------------|

func (f *formatter) createFile(path string) (file *os.File, err error) {
	_ = os.MkdirAll(filepath.Dir(path), os.ModePerm)

	file, err = os.Create(path)
	if err != nil || f.testConfig[testSimulateFailedFileCreation] {
		return nil, createError(ErrorOutputPermission, path)
	}

	return file, nil
}

func (f *formatter) renderType(types []*Data) {
	for _, t := range types {
		f.renderHeader(t.Header)
		f.canva.ParseCode(t.Codes)
		f.canva.ParseSynopsis(t.Synopsis)
		f.canva.ParseNewLine()
		f.renderExample(t.Examples.Examples, headerNone, false)
		f.renderDecl(t.Constants.List, headerNone)
		f.renderDecl(t.Variables.List, headerNone)
		f.renderFunction(t.Functions.List, headerNone, true)
		f.renderFunction(t.Methods.List, headerNone, true)
	}
}

func (f *formatter) renderFunction(functions []*DataElement,
	title string, isType bool) {
	if len(functions) == 0 {
		return
	}

	f.renderHeader(title)

	for i, function := range functions {
		if i > 0 || isType {
			f.renderSubSectionSpacing()
		}

		f.canva.ParseFunction(function.Header,
			function.Receiver,
			function.Value,
		)
		f.renderExample(function.Examples, headerNone, false)
	}
}

func (f *formatter) renderExample(examples []*DataElement,
	title string,
	isPkg bool) {
	if len(examples) == 0 {
		return
	}

	f.renderHeader(title)

	for i, example := range examples {
		if i > 0 || !isPkg {
			f.canva.ParseNewLine()
		}

		f.canva.ParseExample(example.Receiver,
			example.Header,
			example.Synopsis,
			example.Codes,
			example.Value,
		)
	}
}

func (f *formatter) renderDecl(d []*DataElement, title string) {
	if len(d) == 0 {
		return
	}

	f.renderHeader(title)

	for i, data := range d {
		f.canva.ParseSynopsis(data.Synopsis)
		f.canva.ParseNewLine()
		f.canva.BeginDeclGroup(data.Header)

		for j, v := range data.List {
			if j > 0 {
				f.canva.ParseNewLine()
			}

			f.canva.ParseDeclElements(v.Header, v.Value)
		}

		f.canva.EndDeclGroup()

		if i != len(d)-1 {
			f.canva.ParseNewLine()
		}
	}
}

func (f *formatter) renderHeader(title string) {
	if title == "" {
		return
	}

	f.renderSectionSpacing()
	f.canva.ParseHeader(title)
}

func (f *formatter) renderSectionSpacing() {
	f.canva.ParseNewLine()
	f.canva.ParseNewLine()
	f.canva.ParseNewLine()
	f.canva.ParseNewLine()
}

func (f *formatter) renderSubSectionSpacing() {
	f.canva.ParseNewLine()
	f.canva.ParseNewLine()
	f.canva.ParseNewLine()
}
