<!--
+++
date = "2020-03-19T10:16:35+08:00"
title = "In General"
description = """
Wanted to use your own template renderer? Godocgen supports a variety of
templates. Check this page for getting to know-how.
"""
keywords = ["General", "Godocgen"]
authors = ["Godocgen Team"]
draft = false
type = ""
layout = "single"
# thumbnailURL = "#"

[menu.main]
parent = "C) Templates"
weight = 1
+++
-->
# Godocgen Templating in General
Godocgen provides documentation rendering using a template file containing
all the template fragments. This way, it facilitates the flexibility for the
users to create his/her own version of template file. This section guides you
on how to use the template.




## Command Parameters
There are 2 different parameters to trigger the template mode.


### Template Argument
Godocgen facilitates the use of template to render your output using the
`--template` argument. It accepts the filepath pointing to a qualified template
file.

Example, to render a different output using a template file, the command is:

```bash
$ godocgen --template /path/to/template/file \
	--output terminal \
	--input .
```

The output will be rendered based on the availability of the template fragments
from your template file.



### Filename Argument
By default, Godocgen generate file with the name `index.txt`. Godocgen allows
one to change the filename and file extension from using the `--filename`
argument. When this argument is used and a recognizable format is available,
`--template` argument is **compulsory** for specifying your rendering styles.

Example, to create Markdown file with the name `_index.md`, the command is:

```bash
$ godocgen --filename "_index.md" \
	--template /path/to/template/md \
	--output /path/to/output/dir \
	--input ./...
```

This will replace the default `index.txt` to `_index.md` for all generated
documentations.




## Defining Your Template
Godocgen uses rendering fragments to partially render the entire documentations.
Hence, you need to prepare your templates accordingly. This section explains the
role of each template fragments.

To get to know your format specific template, please check-out the supported
template in the navigation bar.


### Head
Head is the leading document section usually used to render metadata of the
documents. Such example would be Hugo's front-matter. To design your `Head`
template, simply define the following template codes into your template file:

```
{{- define "Head" -}}

...Your codes here...

{{ end -}}
```

> **NOTE**: the dashes are for trimming whitespace (tab/newline/space) before or
> after Go's rendering tag.


There are a number of dataset generated by Godocgen at your disposal:

1. `.Header` - Title of the package. Example `cmdline`.
2. `.HugoTimestamp` - RFC3339 formatted timestamp suitable for Hugo date field.
    Example: `2020-03-19T10:16:35+08:00`
3. `.Name` - package name with relative pathing. Example: `api/cmdline`.
4. `.Synopsis` - Short description about the package for SEO usage.
5. `.Timestamp` - RFC1123Z formatted timestamp.
    Example: `Mon, 02 Jan 2006 15:04:05 -0700`.


### Title
Title template fragment renders the document title. To use it, simply define the
template codes into your template file:

```
{{- define "Title" -}}
...Your codes here...
{{ end }}
```

There are a number of dataset generated by Godocgen at your disposal:

1. `.Header` - The "name type". Example: `Package`.
2. `.Name` - The package name value. Example `godocgen`.

Combining them in the following pattern:

```
{{ .Header }} {{ .Name }}
```

Produces:

```
Package godocgen
```


### ImportPath
ImportPath is to render the package's import path. To use it, simply define the
template codes into your template file:

```
{{- define "ImportPath" -}}
...Your codes here...
{{ end }}
```

There are a number of dataset generated by Godocgen at your disposal:

1. `.Value` - Go import path statement

Processing the following pattern:

```
{{ .Value }}
```

Produces:

```
relpath/godocgen
```


### Synopsis
Synopsis is the text contents for the main section, usually after the title and
import path. To use it, simply define the following template codes into your
template file:

```
{{- define "Synopsis" -}}
...your codes here.
{{ end -}}
```

There are a number of dataset generated by Godocgen at your disposal:

1. `.Synopsis` - the contents data.

Processing the following pattern:

```
{{ .Synopsis }}
```

Produces:

```
Package godocgen is a .....

...
```

### Header
Header is the primary section title for each sections. They are usually in
a form of `Constants`, `Functions`, etc. To use it, simply define the following
template codes into your template file:

```
{{- define "Header" }}
...Your codes here...
{{ end }}
```

There are a number of dataset generated by Godocgen at your disposal:

1. `.Name` - value of the header.

Processing the following pattern:

```
{{ .Name }}
```

Produces:

```
Constants
```

### DeclGroup
DeclGroup, known as "declaration group" is the header for creating group
declrations like `const (` or `var (`. It is commonly seen for constants and
variable group. To use it, simply define the template codes into your template
file:

```
{{- define "DeclGroup" }}
...Your codes here...
{{ end }}
```

There are a number of dataset generated by Godocgen at your disposal:

1. `.Name` - name of the group. Example: `const` or `var`.

Processing the following pattern:

```
{{ .Name }} (
```

Produces:

```
const (
```


### DeclElements
DeclElements, known as "declaration group elements" are the individual items
inside the declration group. To use it, simply define the template codes into
your template file:

```
{{- define "DeclElements" }}
...Your codes here...
{{ end -}}
```

There are a number of dataset generated by Godocgen at your disposal:

1. `.Header` - the name or comment section of the element.
2. `.Value` - the element's code body.

Hence, given that the declaration is as follows:

```go
const (
	// Pi is the circular constant
	Pi = 3.142
)
```

With a pattern as such, it will generate the following outcome:
```
{{ .Header }}
{{ .Value }}
```

Output as:

```
// Pi is the circular constant
Pi = 3.142
```

### EndDeclGroup
EndDeclGroup, known as "end declaration group" is the closure for an opening
declration group. To use it, simply define the following template codes into
your template file:

```
{{- define "EndDeclGroup" }}
...Your codes here...
{{ end }}
```

There are no dataset for this field.

Processing the following pattern:

```
)
```

Produces:

```
)
```


### Function
Function is to render a given function (with and without receiver) and its
description. To use it, simply define the following template codes into your
template file:

```
{{- define "Function" -}}
...Your codes here...
{{ end -}}
```

There are a number of dataset generated by Godocgen at your disposal:

1. `.Header` - the synopsis of the function.
2. `.Value` - the function calls (with receiver if available), without the
    `func` title.
3. `.HasReceiver` - the receiver value if the function is an interfacing
   function.

Hence, given the functions as follow:
```
// SayHello is a function to retrun greeting statement
func SayHello() string {
  ...
}

// Greet is the MyTypeReceiver's greeting function returning greeting statement.
func (d *MyTypeReceiver) Greet() string {
  ...
}
```

For pattern as such:

```
TYPE {{ .HasReceiver }} | FUNC {{ .Value }}
{{ .Header }}
```

You will get:
```
TYPE | FUNC SayHello() string
SayHello is a function to retrun greeting statement

TYPE *MyTypeReceiver | FUNC (d *MyTypeReceiver) Greet() string
Greet is the MyTypeReceiver's greeting function returning greeting statement.
```


### Code
Code is to render a given code block. This is commonly used for `Type`
definition where it contains the `struct` definitions. To use it, simply define
the following template codes into your template file:


```
{{- define "Code" -}}
...Your code here...
{{ end -}}
```

There are a number of dataset generated by Godocgen at your disposal:

1. `.Codes` - the string array containing codes line by line.

To use it, you need to loop it over as such:

```
{{- range $i, $line := .Codes }}
Line {{ $i -}}: {{ $line }}
{{- end }}
```

This will produce something like:

```
Line 0: type MyData struct {
Line 1:         data string
Line 2: }
```


### NewLine
NewLine is to render custom new line pattern. This is commonly used for explict
new line separation. Designer can add horizontal line or other styling when
this template is called. To use it, simply define the following template codes
into your template file:

```
{{- define "NewLine" -}}
...Your code here...
{{ end -}}
```

Processing the following pattern:

```
{{- define "NewLine" -}}
----

{{ end -}}
```

Produces:

```
----

```


### Example
Example is to render the custom example code blocks. This is commonly used for
explicit explaination on how to use a particular tool and is applicable to
package, type, function, and methods.

Example template is **ONLY** available starting from `v0.0.2` onwards.

To use it, simply define the following template codes into your template file:

```
{{- define "Example -}}
...Your code here...
{{ end -}}
```

There are a number of parameters available at your disposal:

1. `.Header` - the numbering suffix for multiple examples. Example `First`,
              `Second`, etc.
2. `.Name`   - the name of the associated data (package, type, etc.). For
               package, it is empty.
3. `.Synopsis` - the description of the examples. Optional.
4. `.Codes` - the code blocks of the examples.
5. `.Value` - the output of the example. This is optional depending on the way
              example(s) were written.
