# Description
Describe WHAT is the documentation problem.



## Where is the Problem
### Link
Copy and Paste Your Link here.

### Version
State the current version.



## Current State
Explain the current problem.



## Expected State
Explain the expected outcome.



## Severity
Level of Security: ***Significant***

<!--
OPTIONS

***Critical***
danger, hang, freeze, or kill computer

***Severe***
blocking and can't use. Can't workaround it.

***Significant***
quite a problem but still usable. Can workaround it.

***Not significant***
just some minor touch-up. Everything is fine.
-->



## Urgency
Level of Urgency: ***Code BLUE***

<!--
AVAILABLE OPTIONS

***Code BLACK***
Somebody's life is at stake (e.g. medical equipment, national security)

***Code RED***
Immediate attention! (e.g. my business is not running at all)

***Code BLUE***
Please plan out. (e.g. I can survive for now)

***Code GREEN***
Not urgent. (e.g. take your time)
-->


[comment]: # (Automation - Don't worry! Let's Cory takes over here.)
/label ~"Discussion" ~"Documentation"
