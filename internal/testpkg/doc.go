// Copyright Statement
// Bla bla bla bla
// This should not be documented into the godocgen AT ALL!

// Package testpkg is a test package for godocgen to test on.
//
// It is a simple package holding all known permutations and combinations
// of Go documentation styles. Then, godocgen will generate an output using this
// testpkg package.
//
// Testpkg is an internal package which is **exclusive only** for Godocgen to
// import from. External third-party importing is prohibited by Go build system.
package testpkg
