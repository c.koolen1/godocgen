// Package generator is the internal package for Godocgen to generate doc data.
//
// Generator holds all the go generate processing functions from the standard
// Go package. It processes the packages data and return it to Godocgen for
// its data populations.
//
// Generator package is an internal package which is **exclusive only** for
// Godocgen to import from. External third-party importing is prohibited by Go
// build system.
package generator
