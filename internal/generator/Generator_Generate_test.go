package generator

import (
	"testing"
)

func TestGeneratorGenerate(t *testing.T) {
	scenarios := getTestScenarios()

	for i, s := range scenarios {
		if s.TestType != testGenerate {
			continue
		}

		// prepare
		th := s.prepareTHelper(t)
		s.prepareTestEnvironment()
		path, verdict := s.createPath()

		g := &DocData{}
		ret := g.Configure(path)

		if s.Switches[insertBadPath] {
			g.Path = path
		}

		verdict = s.configureGoMod(g, verdict)

		// test
		g.Generate()

		// verdict
		th.ExpectUIDCorrectness(i, s.UID, false)
		th.ExpectExists("package", g.Package != nil, verdict)
		s.log(th, map[string]interface{}{
			"expected verdict": verdict,
			"given path":       path,
			"targeted path":    g.Path,
			"got verdict":      ret,
			"got package":      g.Package,
			"got error":        g.Err,
		})
		s.cleanUp()
		th.Conclude()
	}
}
