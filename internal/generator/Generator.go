package generator

import (
	"bufio"
	"go/ast"
	"go/build"
	"go/doc"
	"go/parser"
	"go/token"
	"io/ioutil"
	"os"
	"path/filepath"
	"strings"
)

// DocData is the structure for holding a given Go package's documents data.
//
// The data stored within this structure are strictly from go generate standard
// package.
type DocData struct {
	Path         string
	Name         string
	Err          error
	Package      *doc.Package
	BuildPackage *build.Package
	FileSet      *token.FileSet

	// for testing
	testGoMod             string
	testFailedFileParsing bool
}

// Configure is to create go/build package data based on the given path.
//
// It returns `true` when it is done successfully. Otherwise, it returns `false`
// for any error or voodoo outcome.
func (d *DocData) Configure(path string) (isBad bool) {
	pkg, err := build.Default.ImportDir(path, build.ImportComment)
	if err != nil || pkg.Dir == "" {
		return false
	}

	d.Path = pkg.Dir
	_, d.Name = filepath.Split(path)
	d.BuildPackage = pkg

	return true
}

// Generate is to create the go/doc data from a given Go package.
//
// This function will create the actual `go/doc` package data based on the
// `DocData.BuildPackage` and `DocData.FileSet` generated from `Configure(...)`.
// Therefore, one should execute `Configure(...)` before `Generate()`.
func (d *DocData) Generate() {
	var files []os.FileInfo
	var target *ast.File
	var pkg *doc.Package

	// process import path
	importPath := d.processImportPath()

	// setup new FileSet
	d.FileSet = token.NewFileSet()

	// scan for all files
	files, d.Err = ioutil.ReadDir(d.Path)
	if d.Err != nil {
		return
	}

	// parse Go source codes to AST files
	fList := []*ast.File{}

	for _, file := range files {
		// exclude all non-go files
		if !strings.HasSuffix(file.Name(), ".go") {
			continue
		}

		// parse source code to AST file
		target, d.Err = parser.ParseFile(d.FileSet,
			filepath.Join(d.Path, file.Name()),
			nil,
			parser.ParseComments)
		if d.Err != nil || d.testFailedFileParsing {
			continue
		}

		// add successful artifact into list
		fList = append(fList, target)
	}

	// create package's documentations
	if len(fList) == 0 {
		return
	}

	pkg, d.Err = doc.NewFromFiles(d.FileSet, fList, importPath)
	if d.Err == nil {
		d.Package = pkg
	}
}

func (d *DocData) processImportPath() string {
	// go generate somehow does not create proper import path with relative
	// dot path. Hence, we need to process it manually by checking out the
	// closest go.mod file.
	//
	// Otherwise, we assume the package is inside GOPATH.
	var err error
	var modPath string
	var info os.FileInfo

	isFirst := true
	path, _ := filepath.Abs(d.Path)
	filename := goModFilename
	tailingFilepath := ""
	importPath := ""

	if d.testGoMod != "" {
		filename = d.testGoMod
	}

	for {
		// nothing left to check. break the loop.
		if path == "/" {
			break
		}

		if !isFirst {
			path = filepath.Dir(path)
		}

		isFirst = false

		// construct go.mod pathing from current to root directory
		modPath = filepath.Join(path, filename)

		// search for the mod file
		info, err = os.Stat(modPath)
		if os.IsNotExist(err) {
			goto skipLoop
		}

		if info.IsDir() {
			goto skipLoop
		}

		// there is mod file. Parse the import statement
		importPath = d.parseImportPath(modPath)
		if importPath == "" {
			goto skipLoop
		}

		// obtained import path, update data variable and get out
		if tailingFilepath != "" {
			importPath = importPath + "/" + tailingFilepath
		}

		return importPath

	skipLoop:
		if tailingFilepath == "" {
			tailingFilepath = filepath.Base(path)
			continue
		}

		tailingFilepath = filepath.Base(path) + "/" + tailingFilepath
	}

	// no go.mod, use filepath instead
	return d.Path
}

func (d *DocData) parseImportPath(goModPath string) string {
	f, _ := os.Open(goModPath)

	defer func() { _ = f.Close() }()

	scanner := bufio.NewScanner(f)
	for scanner.Scan() {
		if strings.HasPrefix(scanner.Text(), "module") {
			sl := strings.Split(scanner.Text(), " ")
			return strings.TrimSpace(sl[1])
		}
	}

	return ""
}
