package renderer

// Template data labels are the string labels for mapping data in the Go
// template module.
const (
	// NameLabel is the template data label for Name. To use it, simply
	// set {{ .Name }}
	NameLabel = "Name"

	// HeaderLabel is the template data label for Header. To use it, simply
	// set {{ .Header }}
	HeaderLabel = "Header"

	// SynopsisLabel is the template data label for Synopsis. To use it,
	// simply set {{ .Synopsis }}
	SynopsisLabel = "Synopsis"

	// ValueLabel is the template data label for Value. To use it, simply
	// set {{ .Value }}
	ValueLabel = "Value"

	// CodesLabel is the template data label for code blocks. To use it,
	// simply loop over {{ .Codes }} and print out individual lines.
	CodesLabel = "Codes"

	// HasReceiverLabel is the template data label for HasReceiver. To use
	// it, simply set {{ .HasReceiver }}
	HasReceiverLabel = "HasReceiver"

	// HugoTimestampLabel is the template data label for Hugo compatible /
	// RFC3339 timestamp format. To use it, simply set {{ .HugoTimestamp }}
	HugoTimestampLabel = "HugoTimestamp"

	// TimestampLabel is the template data label for RFC1123Z formatted
	// datetime. To use it, simply set {{ .Timestamp }}
	TimestampLabel = "Timestamp"
)

// Template names are the string labels for mapping rendering template
// fragments.
const (
	// TemplateHead is the front matter of the document rendering template
	TemplateHead = "Head"

	// TemplateTitle is the head of the document rendering template
	TemplateTitle = "Title"

	// TemplateHeader is the section header rendering template
	TemplateHeader = "Header"

	// TemplateSynopsis is the synopsis contents rendering template
	TemplateSynopsis = "Synopsis"

	// TemplateDeclGroup is the declaration group opener rendering template
	TemplateDeclGroup = "DeclGroup"

	// TemplateDeclElements is the declration group elements rendering
	// template
	TemplateDeclElements = "DeclElements"

	// TemplateEndDeclGroup is the declration group closer rendering
	// template
	TemplateEndDeclGroup = "EndDeclGroup"

	// TemplateFunction is the function rendering template
	TemplateFunction = "Function"

	// TemplateCode is the code blocks rendering template
	TemplateCode = "Code"

	// TemplateNewLine is the new line rendering template
	TemplateNewLine = "NewLine"

	// TemplateImportPath is the import path rendering template
	TemplateImportPath = "ImportPath"

	// TemplateExample is the example rendering template
	TemplateExample = "Example"
)

// DocType constants are the string labels for document format types.
const (
	// DocTypeNone is the string label for no extension file output
	DocTypeNone = ""

	// DocTypeTerminal is the string label for markdown file output
	DocTypeMarkdown = "md"

	// DocTypeTerminal is the string label for text file output
	DocTypeText = "txt"

	// DocTypeTerminal is the string label for terminal output
	DocTypeTerminal = "terminal"
)
