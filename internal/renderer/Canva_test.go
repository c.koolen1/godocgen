package renderer

import (
	"bufio"
	"io"
	"os"
	"strings"
	"testing"

	"gitlab.com/zoralab/cerigo/testing/thelper"
)

const (
	// switches
	useTerminalSetup         = "useTerminalSetup"
	useTextSetup             = "useTextSetup"
	useMarkdownSetup         = "useMarkdownSetup"
	useNoExtensionSetup      = "useNoExtensionSetup"
	useUnknownExtensionSetup = "useUnknownExtensionSetup"

	// values
	textSetup             = "./dir/index.txt"
	markdownSetup         = "./dir/index.md"
	noExtensionSetup      = "./dir/index"
	unknownExtensionSetup = "./dir/index.whatever"
)

const (
	// switches
	useBadTemplatePath    = "useBadTemplatePath"
	useEmptyTemplatePath  = "useEmptyTemplatePath"
	useProperTemplatePath = "useProperTemplatePath"

	// values
	badTemplatePath    = "./nosuchTemplateman"
	properTemplatePath = "./template"
	emptyTemplatePath  = ""
)

const (
	// switches
	useProperWidth = "useProperWidth"
	useZeroWidth   = "useZeroWidth"

	// values
	properWidth = 70
	zeroWidth   = 0
)

const (
	// switches
	setEmptyHeader   = "setEmptyHeader"
	setEmptyName     = "setEmptyName"
	setEmptySynopsis = "setEmptySynopsis"
	setEmptyValue    = "setEmptyValue"
	setEmptyCodes    = "setEmptyCodes"
	setEmptyReceiver = "setEmptyReceiver"
)

const (
	testIsSupportedFormat = "testIsSupportedFormat"
	testNew               = "testNew"
	testParse             = "testParse"
)

const (
	outputFilepath = "./out.log"
)

type testScenario thelper.Scenario

type testWriter struct {
	Writer io.Writer
	Buffer *bufio.Writer
	File   *os.File
}

func (w *testWriter) Create() error {
	file, err := os.Create(outputFilepath)
	if err != nil {
		return err
	}

	w.Buffer = bufio.NewWriter(file)
	w.Writer = file
	w.File = file

	return nil
}

func (w *testWriter) Close() {
	w.Buffer.Flush()

	if w.File != nil {
		w.File.Close()
	}
}

func (s *testScenario) PrepareTHelper(t *testing.T) *thelper.THelper {
	return thelper.NewTHelper(t)
}

func (s *testScenario) Log(th *thelper.THelper, data map[string]interface{}) {
	th.LogScenario(thelper.Scenario(*s), data)
}

func (s *testScenario) GenerateSetup() (path string, doctype string) {
	switch {
	case s.Switches[useTerminalSetup]:
		path = DocTypeTerminal
		doctype = DocTypeTerminal
	case s.Switches[useTextSetup]:
		path = textSetup
		doctype = DocTypeText
	case s.Switches[useMarkdownSetup]:
		path = markdownSetup
		doctype = DocTypeMarkdown
	case s.Switches[useNoExtensionSetup]:
		path = noExtensionSetup
		doctype = DocTypeNone
	case s.Switches[useUnknownExtensionSetup]:
		path = unknownExtensionSetup
		doctype = DocTypeNone
	default:
	}

	return path, doctype
}

func (s *testScenario) CreateWriter(th *thelper.THelper) (writer *testWriter) {
	writer = &testWriter{}

	err := writer.Create()
	if err != nil {
		th.Errorf("failed to create writer: %v", err)
		return nil
	}

	return writer
}

func (s *testScenario) CloseWriter(writer *testWriter) {
	if writer != nil {
		writer.Close()
	}
}

func (s *testScenario) CreateCanvaParameters() (templatePath string,
	docType string,
	width uint) {
	_, docType = s.GenerateSetup()

	switch {
	case s.Switches[useEmptyTemplatePath]:
		templatePath = emptyTemplatePath
	case s.Switches[useProperTemplatePath]:
		templatePath = properTemplatePath
	case s.Switches[useBadTemplatePath]:
		templatePath = badTemplatePath
	}

	switch {
	case s.Switches[useProperWidth]:
		width = properWidth
	case s.Switches[useZeroWidth]:
		width = zeroWidth
	}

	return templatePath, docType, width
}

func (s *testScenario) ScanNewOutput(th *thelper.THelper,
	c *Canva,
	docType string) {
	// remove all nil cases
	switch {
	case s.Switches[useNoExtensionSetup] &&
		s.Switches[useProperTemplatePath]:
		fallthrough
	case s.Switches[useBadTemplatePath]:
		fallthrough
	case s.Switches[useEmptyTemplatePath] &&
		!s.Switches[useTerminalSetup]:
		if c != nil {
			th.Errorf("unexpected canva exists.")
		}

		return
	}

	// canva must exist at this point
	if c == nil {
		th.Errorf("missing canva.")
		return
	}

	// check engine
	s.checkTextEngine(th, c.engine)
	s.checkTerminalEngine(th, c.engine)
}

func (s *testScenario) checkTextEngine(th *thelper.THelper,
	engine renderFunctions) {
	var x *textParser
	var ok bool

	x, ok = engine.(*textParser)
	if !ok {
		switch {
		case s.Switches[useMarkdownSetup],
			s.Switches[useTextSetup]:
			th.Errorf("failed to convert back into *testParser")
		default:
		}

		return
	}

	switch {
	case s.Switches[useMarkdownSetup]:
	case s.Switches[useTextSetup]:
	case s.Switches[useTerminalSetup] && s.Switches[useProperTemplatePath]:
	default:
		th.Errorf("wrong engine created")
	}

	s.checkEngineWriter(th, x.buffer)
	s.checkEngineWidth(th, x.width)
}

func (s *testScenario) checkTerminalEngine(th *thelper.THelper,
	engine renderFunctions) {
	var x *terminal
	var ok bool

	x, ok = engine.(*terminal)
	if !ok {
		switch {
		case s.Switches[useTerminalSetup] &&
			s.Switches[useEmptyTemplatePath]:
			th.Errorf("failed to convert back into *terminal")
		default:
		}

		return
	}

	switch {
	case s.Switches[useTerminalSetup]:
	default:
		th.Errorf("wrong engine created")
	}

	s.checkEngineWriter(th, x.buffer)
	s.checkEngineWidth(th, x.width)
}

func (s *testScenario) checkEngineWriter(th *thelper.THelper, w io.Writer) {
	switch w.(type) {
	case nil:
		th.Errorf("writer is missing")
		return
	default:
	}

	if w == nil {
		th.Errorf("writer is missing")
		return
	}
}

func (s *testScenario) checkEngineWidth(th *thelper.THelper, width uint) {
	if width == 0 {
		th.Errorf("zero width was not handled")
	}
}

func (s *testScenario) ScanNewError(th *thelper.THelper, err error) {
	expect := false

	switch {
	case s.Switches[useUnknownExtensionSetup]:
		expect = true
	case s.Switches[useNoExtensionSetup]:
		expect = true
	case s.Switches[useBadTemplatePath]:
		expect = true
	case s.Switches[useEmptyTemplatePath] && !s.Switches[useTerminalSetup]:
		expect = true
	default:
	}

	if expect {
		if err == nil {
			th.Errorf("Missing error")
		}

		return
	}

	if err != nil {
		th.Errorf("Unexpected error")
	}
}

func (s *testScenario) ExecuteSupportedFormat(th *thelper.THelper) {
	f := s.CreateWriter(th)
	if f == nil {
		return
	}

	templatePath, docType, _ := s.CreateCanvaParameters()

	c, _ := New(f.Writer, templatePath, docType, properWidth)
	header := s.prepareParseHeader()
	synopsis := s.prepareParseSynopsis()
	name := s.prepareParseName()
	receiver := s.prepareParseReceiver()
	codes := s.prepareCodes()
	value := s.prepareValue()

	c.ParseHead(header, synopsis, name)
	c.ParseTitle(header, name)
	c.ParseHeader(name)
	c.ParseSynopsis(synopsis)
	c.BeginDeclGroup(name)
	c.ParseDeclElements(header, value)
	c.EndDeclGroup()
	c.ParseFunction(header, receiver, value)
	c.ParseCode(codes)
	c.ParseNewLine()
	c.ParseImportPath(value)
	c.ParseExample(name, header, synopsis, codes, value)

	f.Close()

	s.getTemplatedTestResult(th)
	s.getTerminalTestResult(th)
}

func (s *testScenario) prepareParseHeader() string {
	if s.Switches[setEmptyHeader] {
		return ""
	}

	return HeaderLabel
}

func (s *testScenario) prepareParseSynopsis() string {
	if s.Switches[setEmptySynopsis] {
		return ""
	}

	return SynopsisLabel
}

func (s *testScenario) prepareParseName() string {
	if s.Switches[setEmptyName] {
		return ""
	}

	return NameLabel
}

func (s *testScenario) prepareParseReceiver() string {
	if s.Switches[setEmptyReceiver] {
		return ""
	}

	return HasReceiverLabel
}

func (s *testScenario) prepareCodes() []string {
	if s.Switches[setEmptyCodes] {
		return []string{}
	}

	return []string{CodesLabel}
}

func (s *testScenario) prepareValue() string {
	if s.Switches[setEmptyValue] {
		return ""
	}

	return ValueLabel
}

func (s *testScenario) getTerminalTestResult(th *thelper.THelper) {
	if !s.Switches[useEmptyTemplatePath] {
		return
	}

	info, err := os.Stat(outputFilepath)
	if os.IsNotExist(err) {
		th.Errorf("terminal output is empty")
	}

	if info.IsDir() {
		th.Errorf("terminal outpus is a directory?!")
	}
}

//nolint:gocyclo
func (s *testScenario) getTemplatedTestResult(th *thelper.THelper) {
	if !s.Switches[useProperTemplatePath] {
		return
	}

	// open output file for read
	file, err := os.Open(outputFilepath)
	if err != nil {
		th.Errorf("failed to open output file")
	}
	defer file.Close()

	reader := bufio.NewReader(file)
	line := ""

	head := map[string]bool{}
	title := map[string]bool{}
	header := map[string]bool{}
	synopsis := map[string]bool{}
	bDecl := map[string]bool{}
	declElements := map[string]bool{}
	eDecl := map[string]bool{}
	functions := map[string]bool{}
	codes := map[string]bool{}
	newLine := map[string]bool{}
	importPath := map[string]bool{}
	examples := map[string]bool{}

	var op map[string]bool

	for {
		line, err = reader.ReadString('\n')
		line = strings.TrimSpace(line)

		switch {
		case line == "<"+TemplateHead+">":
			op = head
		case line == "<"+TemplateTitle+">":
			op = title
		case line == "<Template"+TemplateHeader+">":
			op = header
		case line == "<"+TemplateSynopsis+">":
			op = synopsis
		case line == "<"+TemplateDeclGroup+">":
			op = bDecl
		case line == "<"+TemplateDeclElements+">":
			op = declElements
		case line == "<"+TemplateEndDeclGroup+">":
			op = eDecl
		case line == "<"+TemplateFunction+">":
			op = functions
		case line == "<"+TemplateCode+">":
			op = codes
		case line == "<"+TemplateNewLine+">":
			op = newLine
		case line == "<"+TemplateImportPath+">":
			op = importPath
		case line == "<"+TemplateExample+">":
			op = examples
		case line == "<Header></Header>":
			op[HeaderLabel] = true
		case line == "<Synopsis></Synopsis>":
			op[SynopsisLabel] = true
		case line == "<Name></Name>":
			op[NameLabel] = true
		case line == "<Timestamp></Timestamp>":
			op[TimestampLabel] = true
		case line == "<Value></Value>":
			op[ValueLabel] = true
		case line == "<HasReceiver></HasReceiver>":
			op[HasReceiverLabel] = true
		case line == "<Codes>[]</Codes>":
			op[CodesLabel] = true
		default:
		}

		if err != nil { // EOF
			break
		}
	}

	s.verifyParsedResults(th,
		head,
		title,
		header,
		synopsis,
		bDecl,
		declElements,
		eDecl,
		functions,
		codes,
		newLine,
		importPath,
		examples)
}

//nolint:gocyclo,gocognit
func (s *testScenario) verifyParsedResults(th *thelper.THelper,
	head,
	title,
	header,
	synopsis,
	bDecl,
	declElements,
	eDecl,
	functions,
	code,
	newLine,
	importPath,
	example map[string]bool) {
	if s.Switches[setEmptyHeader] {
		if !head[HeaderLabel] {
			th.Errorf("missing header in head")
		}

		if !title[HeaderLabel] {
			th.Errorf("missing header in title")
		}

		if !declElements[HeaderLabel] {
			th.Errorf("missing header in declElements")
		}

		if !functions[HeaderLabel] {
			th.Errorf("missing header in functions")
		}

		if !example[HeaderLabel] {
			th.Errorf("missing header in header")
		}
	}

	if s.Switches[setEmptySynopsis] {
		if !head[SynopsisLabel] {
			th.Errorf("missing synopsis in head")
		}

		if !synopsis[SynopsisLabel] {
			th.Errorf("missing synopsis in synopsis")
		}

		if !example[SynopsisLabel] {
			th.Errorf("missing synopsis in examples")
		}
	}

	if s.Switches[setEmptyName] {
		if !head[NameLabel] {
			th.Errorf("missing name in head")
		}

		if !title[NameLabel] {
			th.Errorf("missing name in title")
		}

		if !header[NameLabel] {
			th.Errorf("missing name in header")
		}

		if !bDecl[NameLabel] {
			th.Errorf("missing name in bDecl")
		}

		if !example[NameLabel] {
			th.Errorf("missing name in examples")
		}
	}

	if s.Switches[setEmptyReceiver] {
		if !functions[HasReceiverLabel] {
			th.Errorf("missing receiver in functions")
		}
	}

	if s.Switches[setEmptyCodes] {
		if !example[CodesLabel] {
			th.Errorf("missing codes in example")
		}

		if !code[CodesLabel] {
			th.Errorf("missing codes in code")
		}
	}

	if s.Switches[setEmptyValue] {
		if !declElements[ValueLabel] {
			th.Errorf("missing value in declElements")
		}

		if !functions[ValueLabel] {
			th.Errorf("missing value in functions")
		}

		if !importPath[ValueLabel] {
			th.Errorf("missing value in import path")
		}
	}

	if len(eDecl) > 0 {
		th.Errorf("bogus eDecl elements")
	}

	if len(newLine) > 0 {
		th.Errorf("bogus newLine elements")
	}
}
