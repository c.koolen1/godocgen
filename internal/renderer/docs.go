// Package renderer is the internal package to render the output from doc data.
//
// renderer holds all terminal, text/template, and html/template packages
// styling and documentation rendering.
//
// This package is an internal package which is **exclusive only** for Godocgen
// to import from. External third-party importing is prohibited by Go build
// system.
package renderer
