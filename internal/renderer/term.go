package renderer

import (
	"fmt"
	"io"
	"strings"

	"gitlab.com/zoralab/cerigo/strings/strhelper"
)

type terminal struct {
	buffer io.Writer
	width  uint
}

func (r *terminal) reset() error {
	if r.width == 0 {
		r.width = 80
	}

	return nil
}

func (r *terminal) renderHead(name string,
	synopsis string,
	relativeName string) {
}

func (r *terminal) renderTitle(pkg string, name string) {
	fmt.Fprintf(r.buffer, "%s %s\n", strings.ToUpper(pkg), name)
}

func (r *terminal) renderHeader(name string) {
	fmt.Fprintf(r.buffer, "%s\n", strings.ToUpper(name))
}

func (r *terminal) renderSynopsis(synopsis string) {
	fmt.Fprintf(r.buffer, "%s", synopsis)
}

func (r *terminal) renderDeclGroup(name string) {
	r.renderHorizontalLine()
	fmt.Fprintf(r.buffer, "%s (\n", name)
}

func (r *terminal) renderDeclElements(header string, value string) {
	styler := strhelper.Styler{}
	hl := styler.ContentWrap(header, r.width, strhelper.CharUNIXNewLine)
	hla := []string{}

	for _, v := range hl {
		if v == "" || v == "\n" {
			continue
		}

		hla = append(hla, "// "+v)
	}

	h := strings.Join(hla, "\n\t")
	if h != "" {
		fmt.Fprintf(r.buffer, "\t%s\n", h)
	}

	fmt.Fprintf(r.buffer, "\t%s\n", value)
}

func (r *terminal) renderEndDeclGroup() {
	fmt.Fprintf(r.buffer, ")\n")
	r.renderHorizontalLine()
}

func (r *terminal) renderFunction(header string,
	receiver string,
	value string) {
	fmt.Fprintf(r.buffer, "%s\n%s\n", "Func "+value, header)
}

func (r *terminal) renderCode(codes []string) {
	r.renderHorizontalLine()

	for _, s := range codes {
		fmt.Fprintf(r.buffer, "%s\n", s)
	}

	r.renderHorizontalLine()
}

func (r *terminal) renderNewLine() {
	fmt.Fprintf(r.buffer, "\n")
}

func (r *terminal) renderImportPath(path string) {
	fmt.Fprintf(r.buffer, "import \"%s\"\n", path)
}

func (r *terminal) renderExample(name string,
	suffix string,
	doc string,
	codes []string,
	output string) {
	if len(codes) == 0 {
		return
	}

	// render suffix and name if either of them exists.
	switch {
	case suffix != "" && name != "":
		fmt.Fprintf(r.buffer, "%s %s Example:\n", suffix, name)
	case suffix != "" && name == "":
		fmt.Fprintf(r.buffer, "%s Example:\n", suffix)
	case suffix == "" && name != "":
		fmt.Fprintf(r.buffer, "First %s Example:\n", name)
	default:
	}

	// render doc
	if doc != "" {
		fmt.Fprintf(r.buffer, "%s\n", doc)
	}

	// render code blocks
	r.renderHorizontalLine()

	for _, code := range codes {
		fmt.Fprintf(r.buffer, "%s\n", code)
	}

	// render output
	if output == "" {
		r.renderHorizontalLine()
		return
	}

	r.renderNewLine()
	r.renderNewLine()
	fmt.Fprintf(r.buffer, "╔══════╗\n")
	fmt.Fprintf(r.buffer, "║OUTPUT║\n")
	fmt.Fprintf(r.buffer, "╚══════╝\n")
	fmt.Fprintf(r.buffer, "%s", output)
	r.renderHorizontalLine()
}

func (r *terminal) renderHorizontalLine() {
	fmt.Fprintf(r.buffer, "%s\n", strings.Repeat("—", int(r.width)))
}
