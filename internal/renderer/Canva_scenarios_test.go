package renderer

func testScenarios() []testScenario {
	return []testScenario{
		{
			UID:      1,
			TestType: testIsSupportedFormat,
			Description: `
IsSupportFormat is working fine when:
1. a terminal filepath is provided.
`,
			Switches: map[string]bool{
				useTerminalSetup: true,
			},
		}, {
			UID:      2,
			TestType: testIsSupportedFormat,
			Description: `
IsSupportFormat is working fine when:
1. a text filepath is provided.
`,
			Switches: map[string]bool{
				useTextSetup: true,
			},
		}, {
			UID:      3,
			TestType: testIsSupportedFormat,
			Description: `
IsSupportFormat is working fine when:
1. a markdown filepath is provided.
`,
			Switches: map[string]bool{
				useMarkdownSetup: true,
			},
		}, {
			UID:      4,
			TestType: testIsSupportedFormat,
			Description: `
IsSupportFormat is working fine when:
1. a no extension filepath is provided.
`,
			Switches: map[string]bool{
				useNoExtensionSetup: true,
			},
		}, {
			UID:      5,
			TestType: testIsSupportedFormat,
			Description: `
IsSupportFormat is working fine when:
1. an unknown extension filepath is provided.
`,
			Switches: map[string]bool{
				useUnknownExtensionSetup: true,
			},
		}, {
			UID:      6,
			TestType: testNew,
			Description: `
New is working fine when:
1. terminal doctype is provided.
2. empty template path is provided.
3. proper width is provided.
`,
			Switches: map[string]bool{
				useTerminalSetup:     true,
				useEmptyTemplatePath: true,
				useProperWidth:       true,
			},
		}, {
			UID:      7,
			TestType: testNew,
			Description: `
New is working fine when:
1. text doctype is provided.
2. empty template path is provided.
3. proper width is provided.
`,
			Switches: map[string]bool{
				useTextSetup:         true,
				useEmptyTemplatePath: true,
				useProperWidth:       true,
			},
		}, {
			UID:      8,
			TestType: testNew,
			Description: `
New is working fine when:
1. markdown doctype is provided.
2. empty template path is provided.
3. proper width is provided.
`,
			Switches: map[string]bool{
				useMarkdownSetup:     true,
				useEmptyTemplatePath: true,
				useProperWidth:       true,
			},
		}, {
			UID:      9,
			TestType: testNew,
			Description: `
New is working fine when:
1. empty doctype is provided.
2. empty template path is provided.
3. proper width is provided.
`,
			Switches: map[string]bool{
				useNoExtensionSetup:  true,
				useEmptyTemplatePath: true,
				useProperWidth:       true,
			},
		}, {
			UID:      10,
			TestType: testNew,
			Description: `
New is working fine when:
1. terminal doctype is provided.
2. proper template path is provided.
3. proper width is provided.
`,
			Switches: map[string]bool{
				useTerminalSetup:      true,
				useProperTemplatePath: true,
				useProperWidth:        true,
			},
		}, {
			UID:      11,
			TestType: testNew,
			Description: `
New is working fine when:
1. markdown doctype is provided.
2. proper template path is provided.
3. proper width is provided.
`,
			Switches: map[string]bool{
				useMarkdownSetup:      true,
				useProperTemplatePath: true,
				useProperWidth:        true,
			},
		}, {
			UID:      12,
			TestType: testNew,
			Description: `
New is working fine when:
1. text doctype is provided.
2. proper template path is provided.
3. proper width is provided.
`,
			Switches: map[string]bool{
				useTextSetup:          true,
				useProperTemplatePath: true,
				useProperWidth:        true,
			},
		}, {
			UID:      13,
			TestType: testNew,
			Description: `
New is working fine when:
1. empty doctype is provided.
2. proper template path is provided.
3. proper width is provided.
`,
			Switches: map[string]bool{
				useNoExtensionSetup:   true,
				useProperTemplatePath: true,
				useProperWidth:        true,
			},
		}, {
			UID:      14,
			TestType: testNew,
			Description: `
New is working fine when:
1. markdown doctype is provided.
2. bad template path is provided.
3. proper width is provided.
`,
			Switches: map[string]bool{
				useMarkdownSetup:   true,
				useBadTemplatePath: true,
				useProperWidth:     true,
			},
		}, {
			UID:      15,
			TestType: testNew,
			Description: `
New is working fine when:
1. text doctype is provided.
2. bad template path is provided.
3. proper width is provided.
`,
			Switches: map[string]bool{
				useTextSetup:       true,
				useBadTemplatePath: true,
				useProperWidth:     true,
			},
		}, {
			UID:      16,
			TestType: testNew,
			Description: `
New is working fine when:
1. terminal doctype is provided.
2. bad template path is provided.
3. proper width is provided.
`,
			Switches: map[string]bool{
				useTerminalSetup:   true,
				useBadTemplatePath: true,
				useProperWidth:     true,
			},
		}, {
			UID:      17,
			TestType: testNew,
			Description: `
New is working fine when:
1. no extension doctype is provided.
2. bad template path is provided.
3. proper width is provided.
`,
			Switches: map[string]bool{
				useNoExtensionSetup: true,
				useBadTemplatePath:  true,
				useProperWidth:      true,
			},
		}, {
			UID:      18,
			TestType: testNew,
			Description: `
New is working fine when:
1. markdown doctype is provided.
2. proper template path is provided.
3. zero width is provided.
`,
			Switches: map[string]bool{
				useMarkdownSetup:      true,
				useProperTemplatePath: true,
				useZeroWidth:          true,
			},
		}, {
			UID:      19,
			TestType: testNew,
			Description: `
New is working fine when:
1. text doctype is provided.
2. proper template path is provided.
3. zero width is provided.
`,
			Switches: map[string]bool{
				useTextSetup:          true,
				useProperTemplatePath: true,
				useZeroWidth:          true,
			},
		}, {
			UID:      20,
			TestType: testNew,
			Description: `
New is working fine when:
1. terminal doctype is provided.
2. proper template path is provided.
3. zero width is provided.
`,
			Switches: map[string]bool{
				useTerminalSetup:      true,
				useProperTemplatePath: true,
				useZeroWidth:          true,
			},
		}, {
			UID:      21,
			TestType: testNew,
			Description: `
New is working fine when:
1. empty doctype is provided.
2. proper template path is provided.
3. zero width is provided.
`,
			Switches: map[string]bool{
				useNoExtensionSetup:   true,
				useProperTemplatePath: true,
				useZeroWidth:          true,
			},
		}, {
			UID:      22,
			TestType: testNew,
			Description: `
New is working fine when:
1. empty doctype is provided.
2. empty template path is provided.
3. zero width is provided.
`,
			Switches: map[string]bool{
				useMarkdownSetup:     true,
				useEmptyTemplatePath: true,
				useZeroWidth:         true,
			},
		}, {
			UID:      23,
			TestType: testNew,
			Description: `
New is working fine when:
1. text doctype is provided.
2. empty template path is provided.
3. zero width is provided.
`,
			Switches: map[string]bool{
				useTextSetup:         true,
				useEmptyTemplatePath: true,
				useZeroWidth:         true,
			},
		}, {
			UID:      24,
			TestType: testNew,
			Description: `
New is working fine when:
1. terminal doctype is provided.
2. empty template path is provided.
3. zero width is provided.
`,
			Switches: map[string]bool{
				useTerminalSetup:     true,
				useEmptyTemplatePath: true,
				useZeroWidth:         true,
			},
		}, {
			UID:      25,
			TestType: testNew,
			Description: `
New is working fine when:
1. empty doctype is provided.
2. empty template path is provided.
3. zero width is provided.
`,
			Switches: map[string]bool{
				useNoExtensionSetup:  true,
				useEmptyTemplatePath: true,
				useZeroWidth:         true,
			},
		}, {
			UID:      26,
			TestType: testParse,
			Description: `
All Parse function is working fine when:
1. terminal is supported.
2. proper template path is provided.

`,
			Switches: map[string]bool{
				useTerminalSetup:      true,
				useProperTemplatePath: true,
				setEmptyHeader:        false,
				setEmptySynopsis:      false,
				setEmptyName:          false,
				setEmptyReceiver:      false,
				setEmptyCodes:         false,
				setEmptyValue:         false,
			},
		}, {
			UID:      27,
			TestType: testParse,
			Description: `
All Parse function is working fine when:
1. terminal is supported.
2. proper template path is provided.
3. empty name is supplied.
`,
			Switches: map[string]bool{
				useTerminalSetup:      true,
				useProperTemplatePath: true,
				setEmptyHeader:        false,
				setEmptySynopsis:      false,
				setEmptyName:          true,
				setEmptyReceiver:      false,
				setEmptyCodes:         false,
				setEmptyValue:         false,
			},
		}, {
			UID:      28,
			TestType: testParse,
			Description: `
All Parse function is working fine when:
1. terminal is supported.
2. proper template path is provided.
3. empty header is supplied.
`,
			Switches: map[string]bool{
				useTerminalSetup:      true,
				useProperTemplatePath: true,
				setEmptyHeader:        true,
				setEmptySynopsis:      false,
				setEmptyName:          false,
				setEmptyReceiver:      false,
				setEmptyCodes:         false,
				setEmptyValue:         false,
			},
		}, {
			UID:      29,
			TestType: testParse,
			Description: `
All Parse function is working fine when:
1. terminal is supported.
2. proper template path is provided.
3. empty synopsis is supplied.
`,
			Switches: map[string]bool{
				useTerminalSetup:      true,
				useProperTemplatePath: true,
				setEmptyHeader:        false,
				setEmptySynopsis:      true,
				setEmptyName:          false,
				setEmptyReceiver:      false,
				setEmptyCodes:         false,
				setEmptyValue:         false,
			},
		}, {
			UID:      30,
			TestType: testParse,
			Description: `
All Parse function is working fine when:
1. terminal is supported.
2. proper template path is provided.
3. empty receiver is supplied.
`,
			Switches: map[string]bool{
				useTerminalSetup:      true,
				useProperTemplatePath: true,
				setEmptyHeader:        false,
				setEmptySynopsis:      false,
				setEmptyName:          false,
				setEmptyReceiver:      true,
				setEmptyCodes:         false,
				setEmptyValue:         false,
			},
		}, {
			UID:      31,
			TestType: testParse,
			Description: `
All Parse function is working fine when:
1. terminal is supported.
2. proper template path is provided.
3. empty codes is supplied.
`,
			Switches: map[string]bool{
				useTerminalSetup:      true,
				useProperTemplatePath: true,
				setEmptyHeader:        false,
				setEmptySynopsis:      false,
				setEmptyName:          false,
				setEmptyReceiver:      false,
				setEmptyCodes:         true,
				setEmptyValue:         false,
			},
		}, {
			UID:      32,
			TestType: testParse,
			Description: `
All Parse function is working fine when:
1. terminal is supported.
2. proper template path is provided.
3. empty value is supplied.
`,
			Switches: map[string]bool{
				useTerminalSetup:      true,
				useProperTemplatePath: true,
				setEmptyHeader:        false,
				setEmptySynopsis:      false,
				setEmptyName:          false,
				setEmptyReceiver:      false,
				setEmptyCodes:         false,
				setEmptyValue:         true,
			},
		}, {
			UID:      33,
			TestType: testParse,
			Description: `
All Parse function is working fine when:
1. terminal is supported.
2. empty template path is provided.

`,
			Switches: map[string]bool{
				useTerminalSetup:     true,
				useEmptyTemplatePath: true,
				setEmptyHeader:       false,
				setEmptySynopsis:     false,
				setEmptyName:         false,
				setEmptyReceiver:     false,
				setEmptyCodes:        false,
				setEmptyValue:        false,
			},
		}, {
			UID:      34,
			TestType: testParse,
			Description: `
All Parse function is working fine when:
1. terminal is supported.
2. empty template path is provided.
3. empty name is supplied.
`,
			Switches: map[string]bool{
				useTerminalSetup:     true,
				useEmptyTemplatePath: true,
				setEmptyHeader:       false,
				setEmptySynopsis:     false,
				setEmptyName:         true,
				setEmptyReceiver:     false,
				setEmptyCodes:        false,
				setEmptyValue:        false,
			},
		}, {
			UID:      35,
			TestType: testParse,
			Description: `
All Parse function is working fine when:
1. terminal is supported.
2. empty template path is provided.
3. empty header is supplied.
`,
			Switches: map[string]bool{
				useTerminalSetup:     true,
				useEmptyTemplatePath: true,
				setEmptyHeader:       true,
				setEmptySynopsis:     false,
				setEmptyName:         false,
				setEmptyReceiver:     false,
				setEmptyCodes:        false,
				setEmptyValue:        false,
			},
		}, {
			UID:      36,
			TestType: testParse,
			Description: `
All Parse function is working fine when:
1. terminal is supported.
2. empty template path is provided.
3. empty synopsis is supplied.
`,
			Switches: map[string]bool{
				useTerminalSetup:     true,
				useEmptyTemplatePath: true,
				setEmptyHeader:       false,
				setEmptySynopsis:     true,
				setEmptyName:         false,
				setEmptyReceiver:     false,
				setEmptyCodes:        false,
				setEmptyValue:        false,
			},
		}, {
			UID:      37,
			TestType: testParse,
			Description: `
All Parse function is working fine when:
1. terminal is supported.
2. empty template path is provided.
3. empty receiver is supplied.
`,
			Switches: map[string]bool{
				useTerminalSetup:     true,
				useEmptyTemplatePath: true,
				setEmptyHeader:       false,
				setEmptySynopsis:     false,
				setEmptyName:         false,
				setEmptyReceiver:     true,
				setEmptyCodes:        false,
				setEmptyValue:        false,
			},
		}, {
			UID:      38,
			TestType: testParse,
			Description: `
All Parse function is working fine when:
1. terminal is supported.
2. empty template path is provided.
3. empty codes is supplied.
`,
			Switches: map[string]bool{
				useTerminalSetup:     true,
				useEmptyTemplatePath: true,
				setEmptyHeader:       false,
				setEmptySynopsis:     false,
				setEmptyName:         false,
				setEmptyReceiver:     false,
				setEmptyCodes:        true,
				setEmptyValue:        false,
			},
		}, {
			UID:      39,
			TestType: testParse,
			Description: `
All Parse function is working fine when:
1. terminal is supported.
2. empty template path is provided.
3. empty value is supplied.
`,
			Switches: map[string]bool{
				useTerminalSetup:     true,
				useEmptyTemplatePath: true,
				setEmptyHeader:       false,
				setEmptySynopsis:     false,
				setEmptyName:         false,
				setEmptyReceiver:     false,
				setEmptyCodes:        false,
				setEmptyValue:        true,
			},
		}, {
			UID:      40,
			TestType: testParse,
			Description: `
All Parse function is working fine when:
1. text is supported.
2. proper template path is provided.

`,
			Switches: map[string]bool{
				useTextSetup:          true,
				useProperTemplatePath: true,
				setEmptyHeader:        false,
				setEmptySynopsis:      false,
				setEmptyName:          false,
				setEmptyReceiver:      false,
				setEmptyCodes:         false,
				setEmptyValue:         false,
			},
		}, {
			UID:      41,
			TestType: testParse,
			Description: `
All Parse function is working fine when:
1. text is supported.
2. proper template path is provided.
3. empty name is supplied.
`,
			Switches: map[string]bool{
				useTextSetup:          true,
				useProperTemplatePath: true,
				setEmptyHeader:        false,
				setEmptySynopsis:      false,
				setEmptyName:          true,
				setEmptyReceiver:      false,
				setEmptyCodes:         false,
				setEmptyValue:         false,
			},
		}, {
			UID:      42,
			TestType: testParse,
			Description: `
All Parse function is working fine when:
1. text is supported.
2. proper template path is provided.
3. empty header is supplied.
`,
			Switches: map[string]bool{
				useTextSetup:          true,
				useProperTemplatePath: true,
				setEmptyHeader:        true,
				setEmptySynopsis:      false,
				setEmptyName:          false,
				setEmptyReceiver:      false,
				setEmptyCodes:         false,
				setEmptyValue:         false,
			},
		}, {
			UID:      43,
			TestType: testParse,
			Description: `
All Parse function is working fine when:
1. text is supported.
2. proper template path is provided.
3. empty synopsis is supplied.
`,
			Switches: map[string]bool{
				useTextSetup:          true,
				useProperTemplatePath: true,
				setEmptyHeader:        false,
				setEmptySynopsis:      true,
				setEmptyName:          false,
				setEmptyReceiver:      false,
				setEmptyCodes:         false,
				setEmptyValue:         false,
			},
		}, {
			UID:      44,
			TestType: testParse,
			Description: `
All Parse function is working fine when:
1. text is supported.
2. proper template path is provided.
3. empty receiver is supplied.
`,
			Switches: map[string]bool{
				useTextSetup:          true,
				useProperTemplatePath: true,
				setEmptyHeader:        false,
				setEmptySynopsis:      false,
				setEmptyName:          false,
				setEmptyReceiver:      true,
				setEmptyCodes:         false,
				setEmptyValue:         false,
			},
		}, {
			UID:      45,
			TestType: testParse,
			Description: `
All Parse function is working fine when:
1. text is supported.
2. proper template path is provided.
3. empty codes is supplied.
`,
			Switches: map[string]bool{
				useTextSetup:          true,
				useProperTemplatePath: true,
				setEmptyHeader:        false,
				setEmptySynopsis:      false,
				setEmptyName:          false,
				setEmptyReceiver:      false,
				setEmptyCodes:         true,
				setEmptyValue:         false,
			},
		}, {
			UID:      46,
			TestType: testParse,
			Description: `
All Parse function is working fine when:
1. text is supported.
2. proper template path is provided.
3. empty value is supplied.
`,
			Switches: map[string]bool{
				useTextSetup:          true,
				useProperTemplatePath: true,
				setEmptyHeader:        false,
				setEmptySynopsis:      false,
				setEmptyName:          false,
				setEmptyReceiver:      false,
				setEmptyCodes:         false,
				setEmptyValue:         true,
			},
		}, {
			UID:      47,
			TestType: testParse,
			Description: `
All Parse function is working fine when:
1. markdown is supported.
2. proper template path is provided.

`,
			Switches: map[string]bool{
				useMarkdownSetup:      true,
				useProperTemplatePath: true,
				setEmptyHeader:        false,
				setEmptySynopsis:      false,
				setEmptyName:          false,
				setEmptyReceiver:      false,
				setEmptyCodes:         false,
				setEmptyValue:         false,
			},
		}, {
			UID:      48,
			TestType: testParse,
			Description: `
All Parse function is working fine when:
1. markdown is supported.
2. proper template path is provided.
3. empty name is supplied.
`,
			Switches: map[string]bool{
				useMarkdownSetup:      true,
				useProperTemplatePath: true,
				setEmptyHeader:        false,
				setEmptySynopsis:      false,
				setEmptyName:          true,
				setEmptyReceiver:      false,
				setEmptyCodes:         false,
				setEmptyValue:         false,
			},
		}, {
			UID:      49,
			TestType: testParse,
			Description: `
All Parse function is working fine when:
1. markdown is supported.
2. proper template path is provided.
3. empty header is supplied.
`,
			Switches: map[string]bool{
				useMarkdownSetup:      true,
				useProperTemplatePath: true,
				setEmptyHeader:        true,
				setEmptySynopsis:      false,
				setEmptyName:          false,
				setEmptyReceiver:      false,
				setEmptyCodes:         false,
				setEmptyValue:         false,
			},
		}, {
			UID:      50,
			TestType: testParse,
			Description: `
All Parse function is working fine when:
1. markdown is supported.
2. proper template path is provided.
3. empty synopsis is supplied.
`,
			Switches: map[string]bool{
				useMarkdownSetup:      true,
				useProperTemplatePath: true,
				setEmptyHeader:        false,
				setEmptySynopsis:      true,
				setEmptyName:          false,
				setEmptyReceiver:      false,
				setEmptyCodes:         false,
				setEmptyValue:         false,
			},
		}, {
			UID:      51,
			TestType: testParse,
			Description: `
All Parse function is working fine when:
1. markdown is supported.
2. proper template path is provided.
3. empty receiver is supplied.
`,
			Switches: map[string]bool{
				useMarkdownSetup:      true,
				useProperTemplatePath: true,
				setEmptyHeader:        false,
				setEmptySynopsis:      false,
				setEmptyName:          false,
				setEmptyReceiver:      true,
				setEmptyCodes:         false,
				setEmptyValue:         false,
			},
		}, {
			UID:      52,
			TestType: testParse,
			Description: `
All Parse function is working fine when:
1. markdown is supported.
2. proper template path is provided.
3. empty codes is supplied.
`,
			Switches: map[string]bool{
				useMarkdownSetup:      true,
				useProperTemplatePath: true,
				setEmptyHeader:        false,
				setEmptySynopsis:      false,
				setEmptyName:          false,
				setEmptyReceiver:      false,
				setEmptyCodes:         true,
				setEmptyValue:         false,
			},
		}, {
			UID:      53,
			TestType: testParse,
			Description: `
All Parse function is working fine when:
1. markdown is supported.
2. proper template path is provided.
3. empty value is supplied.
`,
			Switches: map[string]bool{
				useMarkdownSetup:      true,
				useProperTemplatePath: true,
				setEmptyHeader:        false,
				setEmptySynopsis:      false,
				setEmptyName:          false,
				setEmptyReceiver:      false,
				setEmptyCodes:         false,
				setEmptyValue:         true,
			},
		},
	}
}

/* Happy Path - adding new parser
		}, {
			UID:      26,
			TestType: testParse,
			Description: `
All Parse function is working fine when:
1. terminal is supported.
2. proper template path is provided.

`,
			Switches: map[string]bool{
				useTerminalSetup:      true,
				useProperTemplatePath: true,
				setEmptyHeader:        false,
				setEmptySynopsis:      false,
				setEmptyName:          false,
				setEmptyReceiver:      false,
				setEmptyCodes:         false,
				setEmptyValue:         false,
			},
		}, {
			UID:      27,
			TestType: testParse,
			Description: `
All Parse function is working fine when:
1. terminal is supported.
2. proper template path is provided.
3. empty name is supplied.
`,
			Switches: map[string]bool{
				useTerminalSetup:      true,
				useProperTemplatePath: true,
				setEmptyHeader:        false,
				setEmptySynopsis:      false,
				setEmptyName:          true,
				setEmptyReceiver:      false,
				setEmptyCodes:         false,
				setEmptyValue:         false,
			},
		}, {
			UID:      28,
			TestType: testParse,
			Description: `
All Parse function is working fine when:
1. terminal is supported.
2. proper template path is provided.
3. empty header is supplied.
`,
			Switches: map[string]bool{
				useTerminalSetup:      true,
				useProperTemplatePath: true,
				setEmptyHeader:        true,
				setEmptySynopsis:      false,
				setEmptyName:          false,
				setEmptyReceiver:      false,
				setEmptyCodes:         false,
				setEmptyValue:         false,
			},
		}, {
			UID:      29,
			TestType: testParse,
			Description: `
All Parse function is working fine when:
1. terminal is supported.
2. proper template path is provided.
3. empty synopsis is supplied.
`,
			Switches: map[string]bool{
				useTerminalSetup:      true,
				useProperTemplatePath: true,
				setEmptyHeader:        false,
				setEmptySynopsis:      true,
				setEmptyName:          false,
				setEmptyReceiver:      false,
				setEmptyCodes:         false,
				setEmptyValue:         false,
			},
		}, {
			UID:      30,
			TestType: testParse,
			Description: `
All Parse function is working fine when:
1. terminal is supported.
2. proper template path is provided.
3. empty receiver is supplied.
`,
			Switches: map[string]bool{
				useTerminalSetup:      true,
				useProperTemplatePath: true,
				setEmptyHeader:        false,
				setEmptySynopsis:      false,
				setEmptyName:          false,
				setEmptyReceiver:      true,
				setEmptyCodes:         false,
				setEmptyValue:         false,
			},
		}, {
			UID:      31,
			TestType: testParse,
			Description: `
All Parse function is working fine when:
1. terminal is supported.
2. proper template path is provided.
3. empty codes is supplied.
`,
			Switches: map[string]bool{
				useTerminalSetup:      true,
				useProperTemplatePath: true,
				setEmptyHeader:        false,
				setEmptySynopsis:      false,
				setEmptyName:          false,
				setEmptyReceiver:      false,
				setEmptyCodes:         true,
				setEmptyValue:         false,
			},
		}, {
			UID:      32,
			TestType: testParse,
			Description: `
All Parse function is working fine when:
1. terminal is supported.
2. proper template path is provided.
3. empty value is supplied.
`,
			Switches: map[string]bool{
				useTerminalSetup:      true,
				useProperTemplatePath: true,
				setEmptyHeader:        false,
				setEmptySynopsis:      false,
				setEmptyName:          false,
				setEmptyReceiver:      false,
				setEmptyCodes:         false,
				setEmptyValue:         true,
			},
*/
